# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QAction, QFileDialog
from PySide2.QtCore import QObject, Signal
from source.app import BeanPool

# controller for main window of the application
#   handles window menu actions
#   menu actions can also be triggered using keyboard shortcuts
class MainWindowController(QObject):
    # Signal
    images_loaded = Signal(object)
    action_run_triggered = Signal(object)
    action_reset_triggered = Signal()

    def __init__(self, window, file_handler, image_list):
        QObject.__init__(self)
        self.__window = window
        self.__image_list = image_list
        self.__file_handler = file_handler
        self.__init_actions()
        self.__init_connections()

    def __init_actions(self):
        self.__file_action = self.__window.findChild(QAction, "actionLoad")
        self.__file_action_dir = self.__window.findChild(
            QAction, "actionLoadDirectory")
        self.__file_save = self.__window.findChild(
            QAction, "actionSave")
        self.__file_save_as = self.__window.findChild(
            QAction, "actionSaveAs")
        self.__file_save_all = self.__window.findChild(
            QAction, "actionSaveAll")
        self.__file_close = self.__window.findChild(
            QAction, "actionClose")
        self.__file_close_all = self.__window.findChild(
            QAction, "actionCloseAll")
        self.__run = self.__window.findChild(
            QAction, "actionRun")
        self.__run_all = self.__window.findChild(
            QAction, "actionRunAll")
        self.__reset = self.__window.findChild(
            QAction, "actionReset")

    def __init_connections(self):
        self.__file_action.triggered.connect(self.__handle_action_load)
        self.__file_action_dir.triggered.connect(self.__handle_action_load_dir)
        self.__file_save.triggered.connect(self.__handle_action_save_file)
        self.__file_save_as.triggered.connect(
            self.__handle_action_save_file_as)
        self.__file_save_all.triggered.connect(self.__handle_action_save_all)
        self.__file_close.triggered.connect(self.__handle_action_close_file)
        self.__file_close_all.triggered.connect(self.__handle_action_close_all)
        self.__run.triggered.connect(
            lambda: self.action_run_triggered.emit(False))
        self.__run_all.triggered.connect(
            lambda: self.action_run_triggered.emit(True))
        self.__reset.triggered.connect(
            lambda: self.action_reset_triggered.emit())

    def __handle_action_load(self, checked):
        if BeanPool.BeanPool().get_central_controller().is_progress_active():
            return
        # check if user selected just one file or more
        paths = QFileDialog.getOpenFileNames(
            self.__window, "Load Image(s)", "~/Desktop/",
            "Images (*.jpg, *.jpeg, *.tiff)")[0]
        res = []
        if isinstance(paths, list):
            if len(paths) < 1:
                return
            self.__file_handler.load_files(paths)
        else:
            res = self.__file_handler.load_file(paths)
            self.images_loaded.emit(res)

    def __handle_action_load_dir(self):
        if BeanPool.BeanPool().get_central_controller().is_progress_active():
            return
        # start loading in a separate thread
        self.__file_handler.load_dir(QFileDialog.getExistingDirectory(
            self.__window, "Select Directory", "~/Desktop/"))

    def handle_action_load_dir_done(self, res):
        self.images_loaded.emit(res)

    def __handle_action_save_file(self):
        image = self.__image_list.get_selected_image()
        self.__file_handler.save_file(image)

    def __handle_action_save_file_as(self):
        self.__file_handler.save_file_as(
            self.__image_list.get_selected_image())

    def __handle_action_save_all(self):
        images = self.__image_list.get_all_images()
        for image in images:
            self.__file_handler.save_file(image)

    def __handle_action_close_file(self):
        self.__image_list.remove_current_image()

    def __handle_action_close_all(self):
        self.__image_list.remove_all()
