# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QLabel, QProgressBar, QPushButton
from PySide2.QtWidgets import QWidget, QGraphicsView, QSlider
from PySide2.QtGui import QImage, QPixmap, QMatrix
from PySide2.QtCore import QObject, Slot, Signal, Qt
from source.graphic import ImageView, ImageScene
from source.app import global_defs
import math

# controller for central part of the application
#   loads image and stats
#   implements image zoom and cursor tracking
class CentralWidgetController(QObject):
    cancelLoad = Signal()

    def __init__(self, centralWidget, temp_fun):
        QObject.__init__(self)
        self.__centralWidget = centralWidget
        self.__temp_fun = temp_fun
        # find required widgets
        self.__centralWidget.findChild(QLabel, "coordLabel")\
            .setText('[' + '   ' + ', ' + '   ' + '] : ' + '      ' + "   ")
        self.__progress_bar = centralWidget.findChild(
            QProgressBar, "progressBar")
        self.__calcel_load_widget = centralWidget.findChild(
            QWidget, "cancelLoadWidget")
        self.__vertical_widget = centralWidget.findChild(
            QWidget, "verticalWidget")
        self.__info_widget = centralWidget.findChild(
            QWidget, "infoWidget")
        self.__progress_label = centralWidget.findChild(
            QLabel, "progressLabel")
        centralWidget.findChild(
            QPushButton, "cancelLoadButton").clicked.connect(self.cancelLoad)
        self.__init_image_view(centralWidget)
        # hide progress indicators at startup
        self.__progress_bar.setVisible(False)
        self.__calcel_load_widget.setVisible(False)

    # initialize image view component that shows image
    def __init_image_view(self, centralWidget):
        self.__image_view = centralWidget.findChild(
            QGraphicsView, "imageView")
        self.__image_scene = ImageScene.ImageScene(global_defs.THERMAL_IMAGE_WIDTH, global_defs.THERMAL_IMAGE_HEIGHT)
        self.__image_view.setScene(self.__image_scene)
        self.__zoom_slider = centralWidget.findChild(QSlider, "horizontalSlider")
        self.__zoom_slider.valueChanged.connect(self.__handleSliderValueChange)
        self.__image_scene.mousePosChanged.connect(self.update_mouse_coords)

    # load thermal image to image view
    @Slot(object)
    def show_image(self, imarray, stats):
        self.__image_scene.set_mapped_size(global_defs.THERMAL_IMAGE_WIDTH, global_defs.THERMAL_IMAGE_HEIGHT)
        self.__image_scene.draw(imarray)
        # fit in real view size
        self.__image_view.fitInView(self.__image_scene.itemsBoundingRect(), Qt.KeepAspectRatio)
        self.__zoom_slider.setValue(self.__scaleToSliderValue())
        self.show_stats(stats)

    # load visible image to image view
    @Slot(object)
    def show_vis_image(self, imarray, stats):
        if not stats['has_visible']:
            self.show_stats(stats)
            return
        self.__image_scene.set_mapped_size(global_defs.VISIBLE_IMAGE_WIDTH, global_defs.VISIBLE_IMAGE_HEIGHT)
        self.__image_scene.draw(imarray)
        self.__image_view.fitInView(self.__image_scene.itemsBoundingRect(), Qt.KeepAspectRatio)
        self.__zoom_slider.setValue(self.__scaleToSliderValue())
        self.show_stats(stats)

    # update temperature under cursor after it moved
    @Slot(object, object)
    def update_mouse_coords(self, x, y):
        try:
            self.__centralWidget.findChild(QLabel, "coordLabel").setText(
                ('[' + '{0:03}' + ', ' + '{1:03}' + '] : ' + '{2:06.2f}' + " °C")
                .format(x, y, round(self.__temp_fun(x, y), 2)))
        except TypeError:
            self.__centralWidget.findChild(QLabel, "coordLabel").setText("")

    # show image temps stats in bottom widget
    @Slot(object)
    def show_stats(self, stats):
        self.__centralWidget.findChild(QLabel, "maxLabel").setText(
            "Max: " + str(stats['max']) + " °C")
        self.__centralWidget.findChild(QLabel, "minLabel").setText(
            "Min: " + str(stats['min']) + " °C")
        self.__centralWidget.findChild(QLabel, "avgLabel").setText(
            "Avg: " + str(stats['mean']) + " °C")
        self.__centralWidget.findChild(QLabel, "filenameLabel").setText(
            "File: " + stats['filename'])
        self.__centralWidget.findChild(QLabel, "objectsLabel").setText(
            "Found Objects: " + str(stats['found_objects']))
        self.__centralWidget.findChild(QLabel, "objectsLabel").repaint()

    # clear info widget after images were removed
    @Slot()
    def clearWidget(self):
        self.__centralWidget.findChild(QLabel, "maxLabel").setText("")
        self.__centralWidget.findChild(QLabel, "minLabel").setText("")
        self.__centralWidget.findChild(QLabel, "avgLabel").setText("")
        self.__centralWidget.findChild(QLabel, "filenameLabel").setText("")
        self.__centralWidget.findChild(QLabel, "objectsLabel").setText("")

    def is_progress_active(self):
        return self.__progress_bar.isVisible()

    def get_progress_bar(self):
        return self.__progress_bar

    def hide_progress_bar(self):
        self.__progress_bar.setVisible(False)
        self.__calcel_load_widget.setVisible(False)

    # make progress bar visible and set label to current operation
    def show_progress_bar(self, operation):
        if operation == 'file':
            self.__progress_label.setText("Loading Files...")
            self.__calcel_load_widget.setVisible(True)
        elif operation == 'detect':
            self.__progress_label.setText("Running Detector...")
        elif operation == 'params':
            self.__progress_label.setText("Setting parameters...")
            self.__calcel_load_widget.setVisible(True)
        self.__progress_bar.setVisible(True)

    @Slot(int)
    def __handleSliderValueChange(self, value):
        scale = self.__sliderValueToScale()
        matrix = QMatrix()
        matrix.scale(scale, scale)
        self.__image_view.setMatrix(matrix)

    def __sliderValueToScale(self):
        return math.pow(2.0, (self.__zoom_slider.value() - self.__zoom_slider.maximum() / 2) / 50.0);

    def __scaleToSliderValue(self):
        return int(math.log2(self.__image_view.matrix().m11()) * 50.0 + self.__zoom_slider.maximum() / 2.0) + 1;
