# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject, Slot
from PySide2.QtWidgets import QLabel
from source.utils import fov

# controller for fileinfo tab
#   loads image metadata to gui
class FileInfoController(QObject):
    def __init__(self, centralWidget):
        QObject.__init__(self)
        self.__init_labels(centralWidget)

    # fetch all the data from RadiometricImage instance
    @Slot(object)
    def refresh_file_info(self, image):
        exif_data = image.get_exif_data()
        self.__camera_label.setText(exif_data.model)
        self.__vendor_label.setText(exif_data.make)
        self.__sn_label.setText(exif_data.serial)
        self.__fov_label.setText(str(
            exif_data.focal_length) + " mm")
        try:
            self.__timestamp_label.setText(exif_data.timestamp)
        except KeyError:
            self.__timestamp_label.setText("")
        self.__gps_alt_label.setText(
            str(round(exif_data.altitude, 2)) + " m")
        self.__gps_lat_label.setText(
            str(round(exif_data.latitude, 2))
            + " " + exif_data.latitude_ref)
        self.__gps_long_label.setText(
            str(round(exif_data.longitude, 2))
            + " " + exif_data.longitude_ref)
        self.__rel_alt_label.setText(str(round(exif_data.distance, 2)) + " m")
        self.__refresh_fov(
            exif_data.focal_length,
            exif_data.distance)
        self.__repaint_labels()

    def __refresh_fov(self, focal_len, distance):
        self.__pix_label.setText(str(round(fov.get_pix_size(focal_len, distance) * 100, 2)) + ' cm')
        self.__hfov_label.setText(str(round(fov.get_hfov(focal_len, distance), 2)) + ' m')
        self.__vfov_label.setText(str(round(fov.get_vfov(focal_len, distance), 2)) + ' m')

    # init references to gui components
    def __init_labels(self, centralWidget):
        self.__camera_label = centralWidget.findChild(QLabel, "cameraLabel")
        self.__vendor_label = centralWidget.findChild(QLabel, "vendorLabel")
        self.__sn_label = centralWidget.findChild(QLabel, "snLabel")
        self.__timestamp_label = centralWidget.findChild(
            QLabel, "timestampLabel")
        self.__gps_lat_label = centralWidget.findChild(QLabel, "latLabel")
        self.__gps_long_label = centralWidget.findChild(QLabel, "longLabel")
        self.__gps_alt_label = centralWidget.findChild(QLabel, "altLabel")
        self.__rel_alt_label = centralWidget.findChild(QLabel, "relAltLabel")
        self.__fov_label = centralWidget.findChild(QLabel, "fovLabel")
        self.__hfov_label = centralWidget.findChild(QLabel, "hfovLabel")
        self.__vfov_label = centralWidget.findChild(QLabel, "vfovLabel")
        self.__pix_label = centralWidget.findChild(QLabel, "pixLabel")

    def __repaint_labels(self):
        for attr, value in self.__dict__.items():
            if isinstance(value, QLabel):
                value.repaint()
