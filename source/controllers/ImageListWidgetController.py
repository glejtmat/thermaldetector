# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject, Signal, Slot
from PySide2.QtWidgets import QTableWidget,\
    QTableWidgetItem, QTableWidgetSelectionRange

# controller for image list tab
#   controls table with image filenames
#   enables file browsing and selection
class ImageListWidgetController(QObject):
    image_selected = Signal(object)

    def __init__(self, centralWidget):
        QObject.__init__(self)
        self.__image_table = centralWidget.findChild(
            QTableWidget, "tableWidget")
        self.__image_table.cellClicked.connect(self.__handle_table_click)
        self.__image_table.itemSelectionChanged.connect(
            self.__handle_selection_change)
        header = self.__image_table.horizontalHeader()
        header.setStretchLastSection(True)

    # load instances from list into table
    @Slot(object)
    def refresh_image_list(self, imageList):
        self.__image_table.clear()
        self.__image_table.setColumnCount(1)
        self.__image_table.setRowCount(len(imageList))
        cnt = 0
        for image in imageList:
            newItem = QTableWidgetItem("%s" % image.get_stats()['filename'])
            self.__image_table.setItem(cnt, 0, newItem)
            cnt += 1

    @Slot(object)
    def highlight_row(self, row):
        selRange = QTableWidgetSelectionRange(row, 0, row, 0)
        origRange = self.__image_table.selectedRanges()
        if len(origRange) > 0:
            if selRange.topRow() == origRange[0].topRow()\
                    and selRange.bottomRow() == origRange[0].bottomRow():
                return
            self.__image_table.setRangeSelected(QTableWidgetSelectionRange(
                0, 0, self.__image_table.rowCount(), 0), False)
        self.__image_table.setRangeSelected(selRange, True)
        self.__image_table.setCurrentCell(row, 0)
        self.__image_table.repaint()

    @Slot(object)
    def __handle_table_click(self, row, column):
        self.image_selected.emit(row)

    @Slot(object)
    def __handle_selection_change(self):
        row = self.__image_table.currentRow()
        if row >= 0:
            self.__handle_table_click(row, 0)
