# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject, Signal, Slot, Qt
from PySide2.QtWidgets import QWidget, QTableWidget, QTableWidgetItem, QLabel, QTableWidgetSelectionRange
from PySide2.QtWidgets import QPushButton, QComboBox, QDoubleSpinBox, QCheckBox
from PySide2.QtWidgets import QHeaderView
from PySide2.QtGui import QImage, QPixmap
from source.app import BeanPool
from PySide2.QtWidgets import QFileDialog
from source.detector import KeypointFinder

# controller for detector tab
#   loads image and metadata to gui
#   implements image zoom and cursor tracking
class DetectorWidgetController(QObject):
    run_button_clicked = Signal(object, object, object)

    def __init__(self, centralWidget):
        QObject.__init__(self)
        self.__run_button = centralWidget.findChild(QPushButton, "runButton")
        self.__run_all_button = centralWidget.findChild(
            QPushButton, "runAllButton")
        self.__export_button = centralWidget.findChild(
                QPushButton, "exportPushButton")
        self.__algComboBox = centralWidget.findChild(QComboBox, "algComboBox")
        self.__thresholdSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "thresholdSpinBox")
        self.__envSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "envSpinBox")
        self.__distSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "distSpinBox")
        self.__simpleDetectorWidget = centralWidget.findChild(
            QWidget, "simpleDetectorWidget")
        self.__resultWidget = centralWidget.findChild(
            QWidget, "resultWidget")
        self.__result_table = centralWidget.findChild(
            QTableWidget, "resultTable")
        self.__keypoint_label = centralWidget.findChild(
                QLabel, "keypointLabel")
        self.__adjust_check = centralWidget.findChild(
            QCheckBox, "adjustCheckBox")
        self.__init_connections()
        self.__init_result_table()

    def __init_connections(self):
        self.__algComboBox.currentTextChanged.connect(
            lambda text: self.__simpleDetectorWidget.setVisible(
                True if text == 'simple' else False)
        )
        self.__run_button.clicked.connect(
            lambda: self.__handle_run_button_click(all=False))
        self.__run_all_button.clicked.connect(
            lambda: self.__handle_run_button_click(all=True))
        self.__result_table.itemSelectionChanged.connect(
            self.__handle_selection_change)
        self.__export_button.clicked.connect(
            self.__handle_export_jpeg)

    def __init_result_table(self):
        header = self.__result_table.horizontalHeader()
        header.setSectionResizeMode(QHeaderView.ResizeToContents)
        self.__result_table.setHorizontalHeaderLabels(
            ['X', 'Y', 'Size', "Min", "Avg", "Max"])

    def __handle_export_jpeg(self):
        imarray = BeanPool.BeanPool().get_image_list().get_selected_image().get_rgb_data()
        image = QImage(imarray, imarray.shape[1], imarray.shape[0],
                   imarray.shape[1] * 3, QImage.Format_RGB888)
        path = QFileDialog.getSaveFileName(
            None, "Save", "~/Desktop/", "Image (*.jpeg))")[0]
        path += ".jpeg"
        image.save(path)

    @Slot(object)
    def __handle_selection_change(self):
        row = self.__result_table.currentRow()
        if row >= 0:
            self.__show_keypoint(self.__obj[row])
        BeanPool.BeanPool().get_image_list().get_selected_image().\
            highlight_keypoint(self.__obj, row, "thermal")
        BeanPool.BeanPool().get_image_list().trigger_image_changed()

    @Slot(object)
    def __handle_run_button_click(self, all):
        if BeanPool.BeanPool().get_central_controller().is_progress_active():
            return
        self.run_button_clicked.emit(
            all, self.__algComboBox.currentText(),
            self.__adjust_check.checkState() == Qt.CheckState.Checked)

    #   load object rgb pixels to view
    @Slot(object)
    def __show_keypoint(self, keypoint):
        finder = KeypointFinder.KeypointFinder()
        image = QImage(keypoint['rgb'], keypoint['rgb'].shape[1],
                       keypoint['rgb'].shape[0], keypoint['rgb'].shape[1] * 3,
                       QImage.Format_RGB888)
        picture = QPixmap(image)
        self.__keypoint_label.setPixmap(picture)
        self.__keypoint_label.repaint()

    # highlight selected row from result table
    def __highlight_row(self, row):
        selRange = QTableWidgetSelectionRange(row, 0, row, 5)
        origRange = self.__result_table.selectedRanges()
        if len(origRange) > 0:
            if selRange.topRow() == origRange[0].topRow()\
                    and selRange.bottomRow() == origRange[0].bottomRow():
                return
            self.__result_table.setRangeSelected(QTableWidgetSelectionRange(
                0, 0, self.__image_table.rowCount(), 5), False)
        self.__result_table.setRangeSelected(selRange, True)
        self.__result_table.setCurrentCell(row, 0)
        self.__result_table.repaint()

    # load detection results to table after it finishes
    @Slot(object)
    def refresh_result_table(self, image):
        self.__resultWidget.setVisible(
            True if image.detection_done() else False)
        self.__result_table.clear()
        self.__result_table.setColumnCount(6)
        self.__obj = image.get_detected_objects()
        self.__result_table.setRowCount(len(self.__obj))
        self.__result_table.setHorizontalHeaderLabels(
            ['X', 'Y', 'Size', "Min", "Avg", "Max"])
        cnt = 0
        for x in self.__obj:
            item0 = QTableWidgetItem("%s" % round(x['x'], 2))
            self.__result_table.setItem(cnt, 0, item0)
            item1 = QTableWidgetItem("%s" % round(x['y'], 2))
            self.__result_table.setItem(cnt, 1, item1)
            item2 = QTableWidgetItem("%s" % round(x['r'], 2))
            self.__result_table.setItem(cnt, 2, item2)
            item3 = QTableWidgetItem("%s" % round(x['min'], 2))
            self.__result_table.setItem(cnt, 3, item3)
            item4 = QTableWidgetItem("%s" % round(x['avg'], 2))
            self.__result_table.setItem(cnt, 4, item4)
            item5 = QTableWidgetItem("%s" % round(x['max'], 2))
            self.__result_table.setItem(cnt, 5, item5)
            cnt += 1

    def clearWidget(self):
        self.__resultWidget.setVisible(False)
        self.__result_table.clear()
        self.__keypoint_label.clear()

    # load settings from current image after image selection changed
    @Slot(object)
    def on_image_selected(self, image):
        self.__envSpinBox.blockSignals(True)
        self.__distSpinBox.blockSignals(True)
        exif_info = image.get_exif_data()
        self.__envSpinBox.setValue(image.get_env_temp())
        self.__distSpinBox.setValue(round(exif_info.distance, 2))
        self.__envSpinBox.blockSignals(False)
        self.__distSpinBox.blockSignals(False)
        self.refresh_result_table(image)

    # getter for chosen detection params
    def get_alg_opt(self):
        return self.__algComboBox.currentText()

    def get_threshold_opt(self):
        return self.__thresholdSpinBox.value()

    def get_env_opt(self):
        return self.__envSpinBox.value()

    def get_distance_opt(self):
        return self.__distSpinBox.value()
