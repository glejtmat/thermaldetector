# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject, Signal, Slot
from PySide2.QtWidgets import QPushButton, QComboBox, QDoubleSpinBox, QWidget
from source.app import BeanPool
from source.app.thread import ToolbarWorker

# controller for toolbar above image
#   manages inputs for palettes and temperatures
class ToolbarWidgetController(QObject):
    visible_toggle_clicked = Signal()
    ir_toggle_clicked = Signal()
    next_button_clicked = Signal()
    prev_button_clicked = Signal()
    palette_limits_changed = Signal(object, object)

    def __init__(self, centralWidget):
        QObject.__init__(self)
        centralWidget.findChild(QWidget, "toolbarWidget")\
            .setStyleSheet('source/style/toolbar_style.css')
        self.__init_buttons(centralWidget)
        self.__init_connections()
        self.__visible_toggle_opt = False

    # action toshow thermal image
    def toggle_IR(self):
        self.__visible_toggle_opt = False
        self.__visible_toggle.setText("Show RGB")
        self.__visible_toggle.repaint()

    def __init_buttons(self, centralWidget):
        self.__next_button = centralWidget.findChild(QPushButton, "nextButton")
        self.__prev_button = centralWidget.findChild(QPushButton, "prevButton")
        self.__visible_toggle = centralWidget.findChild(
            QPushButton, "visibleToggle")
        self.__paletteComboBox = centralWidget.findChild(
            QComboBox, "paletteComboBox")
        self.__maskComboBox = centralWidget.findChild(
            QComboBox, "maskComboBox")
        self.__minSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "minSpinBox")
        self.__maxSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "maxSpinBox")
        self.__maskSpinBox = centralWidget.findChild(
            QDoubleSpinBox, "maskSpinBox")
        self.__apply_all_button = centralWidget.findChild(
            QPushButton, "applyToAllButton")
        self.__auto_adjust_button = centralWidget.findChild(
            QPushButton, "autoAdjustButton")

    def __init_connections(self):
        self.__next_button.clicked.connect(self.__handle_next_button_click)
        self.__prev_button.clicked.connect(self.__handle_prev_button_click)
        self.__apply_all_button.clicked.connect(self.__handle_apply_all_button_click)
        self.__visible_toggle.clicked.connect(
            self.__handle_visible_toggle_click)
        self.__paletteComboBox.currentTextChanged.connect(
            self.__handle_palette_change)
        self.__maskComboBox.currentTextChanged.connect(
            self.__handle_mask_change)
        self.__minSpinBox.valueChanged.connect(self.__handle_pal_lim_change)
        self.__maxSpinBox.valueChanged.connect(self.__handle_pal_lim_change)
        self.__maskSpinBox.valueChanged.connect(self.__handle_mask_change)
        self.__auto_adjust_button.clicked.connect(self.__handle_auto_adjust)

    # trigger auto adjust of selected image
    @Slot(object)
    def __handle_auto_adjust(self):
        try:
            beanPool = BeanPool.BeanPool()
            beanPool.get_image_list().get_selected_image().auto_adjust()
            beanPool.get_image_list().trigger_image_changed()
        except AttributeError:
            print("Could not auto adjust, image not loaded")

    @Slot(object)
    def __handle_mask_change(self):
        beanPool = BeanPool.BeanPool()
        beanPool.get_image_list().get_selected_image().set_mask(
            self.__maskComboBox.currentText(), self.__maskSpinBox.value())
        beanPool.get_image_list().trigger_image_changed()

    @Slot(object)
    def __handle_palette_change(self, text):
        beanPool = BeanPool.BeanPool()
        beanPool.get_image_list().get_selected_image().set_palette(
            text)
        beanPool.get_image_list().trigger_image_changed()

    # load setting from newly loaded image
    @Slot(object)
    def on_image_selected(self, image):
        self.__minSpinBox.blockSignals(True)
        self.__maxSpinBox.blockSignals(True)
        self.__minSpinBox.setValue(image.get_palette_min())
        self.__maxSpinBox.setValue(image.get_palette_max())
        self.__minSpinBox.blockSignals(False)
        self.__maxSpinBox.blockSignals(False)
        self.__paletteComboBox.setCurrentIndex(
            self.__paletteComboBox.findText(image.get_palette_name()))

    @Slot(object)
    def __handle_pal_lim_change(self):
        beanPool = BeanPool.BeanPool()
        beanPool.get_image_list().get_selected_image().set_palette_limits(
            self.__minSpinBox.value(), self.__maxSpinBox.value())
        beanPool.get_image_list().trigger_image_changed()

    @Slot(object)
    def __handle_run_button_click(self, all):
        self.run_button_clicked.emit(
            all, self.__algComboBox.currentText(),
            self.__sourceComboBox.currentText())

    @Slot(object)
    def __handle_prev_button_click(self):
        self.prev_button_clicked.emit()

    @Slot(object)
    def __handle_next_button_click(self):
        self.next_button_clicked.emit()

    # show visible image
    @Slot(object)
    def __handle_visible_toggle_click(self):
        self.__visible_toggle_opt = not self.__visible_toggle_opt
        try:
            if self.__visible_toggle_opt:
                self.__visible_toggle.setText("Show IR")
                pool = BeanPool.BeanPool()
                pool.get_central_controller().show_vis_image(
                    pool.get_image_list().get_selected_image().get_visible_data(),
                    pool.get_image_list().get_selected_image().get_stats())
            else:
                self.__visible_toggle.setText("Show RGB")
                pool = BeanPool.BeanPool()
                pool.get_central_controller().show_image(
                    pool.get_image_list().get_selected_image().get_rgb_data(),
                    pool.get_image_list().get_selected_image().get_stats())
        except AttributeError:
            return

    # apply selected toolbar setting to all the images currently loaded
    @Slot(object)
    def __handle_apply_all_button_click(self, all):
        if BeanPool.BeanPool().get_central_controller().is_progress_active():
            return
        list = BeanPool.BeanPool().get_image_list().get_all_images()
        self.__progressBar = BeanPool.BeanPool().\
            get_central_controller().get_progress_bar()
        self.thread = ToolbarWorker.ToolbarWorker(
            list, self.__paletteComboBox.currentText(),
            self.__minSpinBox.value(), self.__maxSpinBox.value(),
            self.__maskComboBox.currentText(), self.__maskSpinBox.value())
        self.thread.updateProgress.connect(
            lambda progress: self.__progressBar.setValue(progress))
        #self.thread.workDone.connect(
        #    BeanPool.BeanPool().
        #    get_window_controller().handle_action_load_dir_done)
        self.thread.workDone.connect(
            lambda: BeanPool.BeanPool().get_central_controller().
            hide_progress_bar())
        BeanPool.BeanPool().get_central_controller().show_progress_bar('params')
        self.__progressBar.setMinimum(0)
        self.__progressBar.setRange(0, len(list))
        BeanPool.BeanPool().get_central_controller().cancelLoad\
            .connect(self.cancel_load)
        self.thread.start()

    # stop thread
    def cancel_load(self):
        try:
            if not self.thread.isRunning():
                return
        except AttributeError:
            return
        self.thread.stop()
        BeanPool.BeanPool().get_central_controller().hide_progress_bar()

