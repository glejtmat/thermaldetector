# This Python file uses the following encoding: utf-8
from source.detector.ShapeDescriptors import ShapeDescriptors
from source.detector import ImageUtils
import cv2

# collection of methods used to analyze shape of detected keypoints
#   uses shape descriptors
class KeypointAnalyzer:

    @staticmethod
    def form_factor(cont):
        return ShapeDescriptors.form_factor(
            cv2.contourArea(cont), cv2.arcLength(cont, True))

    @staticmethod
    def roundness(cont):
        area = cv2.contourArea(cont)
        _, radius = cv2.minEnclosingCircle(cont)
        r = ShapeDescriptors.roundness(area, 2*radius)
        if r > 1:
            r = 1
        return r

    # Poměr stran
    @staticmethod
    def aspect_ratio(cont):
        center, size, angle = cv2.minAreaRect(cont)
        min_diameter = min(size[0], size[1])
        max_diameter = max(size[0], size[1])
        return ShapeDescriptors.aspect_ratio(min_diameter, max_diameter)

    @staticmethod
    def convexity(cont):
        hull = cv2.convexHull(cont, None, True, True)
        per = cv2.arcLength(cont, True)
        conv_per = cv2.arcLength(hull, True)
        r = 0
        try:
            r = ShapeDescriptors.convexity(per, conv_per)
        except ZeroDivisionError:
            print('zero porimeter')
        if r > 1:
            r = 1
        return r

    @staticmethod
    def solidity(cont):
        hull = cv2.convexHull(cont, None, True, True)
        area = cv2.contourArea(cont)
        conv_area = cv2.contourArea(hull)
        r = 0
        try:
            r = ShapeDescriptors.solidity(area, conv_area)
        except ZeroDivisionError:
            print('null convex area')
        if r > 1:
            r = 1
        return r

    @staticmethod
    def compactness(cont):
        area = cv2.contourArea(cont)
        center, size, angle = cv2.minAreaRect(cont)
        max_diameter = max(size[0], size[1])
        r = ShapeDescriptors.compactness(area, max_diameter)
        if r > 1:
            r = 1
        return r

    # Dosah, rozměrnost
    def extent(cont):
        area = cv2.contourArea(cont)
        center, size, angle  = cv2.minAreaRect(cont)
        return ShapeDescriptors.extent(area, size[0] * size[1])
