# This Python file uses the following encoding: utf-8
from source.detector import DetectorInterface, KeypointFinder
import cv2
import numpy as np

# yolo3v image detector
# uses weights that were created by training
# traing session used pre trained data and fine tuning on thermograms dataset
class YoloDetector(DetectorInterface.DetectorInterface):
    def __init__(self):
        DetectorInterface.DetectorInterface.__init__(self)
        self.__keypoint_finder = KeypointFinder.KeypointFinder()

    # function to get the output layer names
    # in the architecture
    def get_output_layers(self, net):

        layer_names = net.getLayerNames()

        output_layers =\
            [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

        return output_layers

    # function to draw bounding box on the detected object with class name
    def draw_bounding_box(self, img, class_id, confidence, x, y,
                          x_plus_w, y_plus_h, classes):
        label = str(classes[class_id])
        # generate different colors for different classes
        COLORS = np.random.uniform(0, 255, size=(len(classes), 3))
        color = COLORS[class_id]
        cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 2)
        cv2.putText(
            img, label, (x-10, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    def _detect_img(self, radiometric_image):
        print('yolo detect start')
        # handle command line arguments
        classes_path = "detection_helper_files/yolo3/yolov3.txt"
        weight_path = "detection_helper_files/yolo3/yolov3.weights"
        config_path = "detection_helper_files/yolo3/yolov3.cfg"

        # read input image
        image = self._get_cvimage(radiometric_image)

        Width = image.shape[1]
        Height = image.shape[0]
        scale = 0.00392

        # read class names from text file
        classes = None
        with open(classes_path, 'r') as f:
            classes = [line.strip() for line in f.readlines()]

        # uncomment to infere with GPU
        #net.setPreferableBackend(cv2.dnn.DNN_BACKEND_CUDA)
        #net.setPreferableTarget(cv2.dnn. DNN_TARGET_CUDA)
        # read pre-trained model and config file
        net = cv2.dnn.readNet(weight_path, config_path)

        # create input blob
        blob = cv2.dnn.blobFromImage(
            image, scale, (416, 416), (0, 0, 0), True, crop=False)

        # set input blob for the network
        net.setInput(blob)

        # run inference through the network
        # and gather predictions from output layers
        outs = net.forward(self.get_output_layers(net))

        # initialization
        class_ids = []
        confidences = []
        boxes = []
        conf_threshold = 0.1
        nms_threshold = 0.1

        # for each detetion from each output layer
        # get the confidence, class id, bounding box params
        # and ignore weak detections (confidence < 0.5)
        res_keypoints = []
        for out in outs:
            for detection in out:
                scores = detection[5:]
                class_id = np.argmax(scores)
                confidence = scores[class_id]
                if confidence > 0.5:
                    center_x = int(detection[0] * Width)
                    center_y = int(detection[1] * Height)
                    w = int(detection[2] * Width)
                    h = int(detection[3] * Height)
                    x = center_x - w / 2
                    y = center_y - h / 2
                    class_ids.append(class_id)
                    confidences.append(float(confidence))
                    boxes.append([x, y, w, h])

                    # apply non-max suppression
                    indices = cv2.dnn.NMSBoxes(
                        boxes, confidences, conf_threshold, nms_threshold)
                    temp = np.array(
                        self.__keypoint_finder.get_boundingbox_temps([x, y, w, h], radiometric_image))
                    rgb = np.array(
                        self.__keypoint_finder.get_boundingbox_rgb([x, y, w, h], radiometric_image))
                    res_point = {'x': center_x, 'y': center_y,
                        'r': w if w > h else h, "min": temp.min(axis=None),
                        "max": temp.max(axis=None), "avg": temp.mean(axis=None),
                        "rgb": rgb}
                    res_keypoints.append(res_point)
        radiometric_image.set_result_data(res_keypoints, self._image_type)
