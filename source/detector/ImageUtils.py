# This Python file uses the following encoding: utf-8
import cv2
import numpy as np


# utility functions for image manipulation
def resize(im, size):
    return cv2.resize(im, size, cv2.INTER_AREA)


def logical_and(bin_im, bin_mask):
    return cv2.bitwise_and(bin_im, bin_mask)


def crop(img, tl_x, tl_y, br_x, br_y):
    roi = img[tl_y:br_y, tl_x:br_x]
    return roi


def crop_by_bounding_rect(img_bin):
    assert len(img_bin.shape) == 2, 'Input image is NOT binary!'

    contours, _ = cv2.findContours(
        img_bin, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    tl_x, tl_y, w, h = cv2.boundingRect(contours[0])
    return crop(img_bin, tl_x, tl_y, tl_x+w, tl_y+h)


def crop_contour(contour, image):
    x, y, w, h = cv2.boundingRect(contour)
    return image[y:y+h, x:x+w]


def find_contours(img_bin, min_area=0,
                  max_area=1000000, fill=True, external=True):
    mode = cv2.RETR_EXTERNAL
    if not external:
        mode = cv2.RETR_LIST
    contours, _ = cv2.findContours(img_bin, mode, cv2.CHAIN_APPROX_SIMPLE)
    contours = [
        c for c in contours if cv2.contourArea(c) >
        min_area and cv2.contourArea(c) < max_area]
    thick = cv2.FILLED
    if not fill:
        thick = 2
    contour_drawn = cv2.drawContours(np.zeros(
        img_bin.shape, dtype=np.uint8), contours, -1,
        color=(255, 255, 255), thickness=thick)
    return contour_drawn, len(contours), contours
