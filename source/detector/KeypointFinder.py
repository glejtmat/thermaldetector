# This Python file uses the following encoding: utf-8
import cv2
import math
import numpy as np
from PIL import Image
from source.detector import KeypointAnalyzer
from source.app import global_defs

# class used to find blobs when using simple detector
#   uses opencv to find blobs and contours
class KeypointFinder:
    def __init__(self): pass

    # find blobs according to parameters
    def blob_detect(self, im, pixsize):
        # init blob detector with params
        params = cv2.SimpleBlobDetector_Params()
        params.minThreshold = 1
        params.maxThreshold = 1000
        params.filterByArea = True
        params.minArea = .25 / pixsize * .25 / pixsize
        params.maxArea = 0.5 / pixsize * 2 / pixsize
        print('min a: ', params.minArea)
        print('max a: ', params.maxArea)
        params.filterByCircularity = False
        params.minCircularity = 0.1
        params.filterByConvexity = False
        # params.minConvexity = 0.87
        params.filterByInertia = False
        # params.minInertiaRatio = 0.01
        params.filterByColor = True
        params.blobColor = 255
        detector = cv2.SimpleBlobDetector_create(params)
        keypoints = detector.detect(im)
        return keypoints

    # get array of temperatures for each pixel of keypoint
    def get_keypoint_temps(self, keypoint, image):
        center = (keypoint.pt[0], keypoint.pt[1])
        allTemps = image.get_temperatures()
        temps = []

        for i in range(int(center[0] - keypoint.size),
                       int(center[0] + keypoint.size)):
            tempsRow = []
            for j in range(int(center[1] - keypoint.size),
                           int(center[1] + keypoint.size)):
                try:
                    tempsRow.append(round(allTemps[j][i], 2))
                except IndexError:
                    tempsRow.append(0)
                    continue
            temps.append(tempsRow)
        # dump
        return temps

    # filter keypoint according to temperatures
    #   returns True if keypoint passed, False otherwise
    def temp_filter(self, keypoint_temps, max_temp, env_temp):
        max = -273.15
        cnt = 0.0
        sum = 0.0
        for row in keypoint_temps:
            for temp in row:
                if temp > max:
                    max = temp
                if temp > max_temp:
                    return False
                cnt +=1
                sum += temp
        if sum / cnt < env_temp:
            return False
        return True

    # filter keypoint according to shapes
    #   returns True if keypoint passed, False otherwise
    def contour_filter(self, contour):
        convexity = KeypointAnalyzer.KeypointAnalyzer.convexity(contour)
        if convexity >= 1.0 or convexity < global_defs.CONVEXITY_MIN:
            return False
        solidity = KeypointAnalyzer.KeypointAnalyzer.solidity(contour)
        if solidity >= 1.0 or solidity < global_defs.SOLIDITY_MIN:
            return False
        form_factor = KeypointAnalyzer.KeypointAnalyzer.form_factor(contour)
        if form_factor >= 1.0 or form_factor < global_defs.FORM_FACTOR_MIN:
            return False
        aspect_ratio = KeypointAnalyzer.KeypointAnalyzer.aspect_ratio(contour)
        if aspect_ratio >= 1.0 or aspect_ratio < global_defs.ASPECT_MIN:
            return False
        roundness = KeypointAnalyzer.KeypointAnalyzer.roundness(contour)
        if roundness >= 1.0 or roundness < global_defs.ROUNDNESS_MIN:
            return False
        compactness = KeypointAnalyzer.KeypointAnalyzer.compactness(contour)
        if compactness >= 1.0 or compactness < global_defs.COMPACTNESS_MIN:
            return False
        extent = KeypointAnalyzer.KeypointAnalyzer.extent(contour)
        if extent >= 1.0 or extent < global_defs.EXTENT_MIN:
            return False
        print('convexity: {}, solidity: {}, form_factor: {}. aspect_ratio: {}, roundness {}, compactness {}, extent {}'.format(
            convexity, solidity, form_factor, aspect_ratio, roundness, compactness, extent))
        return True

    # get numpy arr with raw values for the rectangle with candidate object
    def get_keypoint_rgb(self, keypoint, image):
        print(keypoint.pt[0], ' ', keypoint.pt[1], flush=True)
        center = (keypoint.pt[0], keypoint.pt[1])
        image_rgb = image.get_rgb_data()
        rgb = []
        for i in range(int(center[0] - keypoint.size),
                       int(center[0] + keypoint.size)):
            rgbRow = []
            for j in range(int(center[1] - keypoint.size),
                           int(center[1] + keypoint.size)):
                try:
                    rgbRow.append(image_rgb[j][i])
                except IndexError:
                    continue
            rgb.append(rgbRow)
        return rgb

    def get_boundingbox_temps(self, box, image):
        allTemps = image.get_temperatures()
        temps = []
        for i in range(int(box[0]),
                       int(box[0] + box[2])):
            tempsRow = []
            for j in range(int(box[1]),
                           int(box[1] + box[2])):
                try:
                    tempsRow.append(round(allTemps[j][i], 2))
                except IndexError:
                    tempsRow.append(0)
                    continue
            temps.append(tempsRow)
        # dump
        return temps

    # get numpy arr with raw values for the rectangle with candidate object
    def get_boundingbox_rgb(self, box, image):
        image_rgb = image.get_rgb_data()
        rgb = []
        for i in range(int(box[0]),
                       int(box[0] + box[2])):
            rgbRow = []
            for j in range(int(box[1]),
                           int(box[1] + box[3])):
                try:
                    rgbRow.append(image_rgb[j][i])
                except IndexError:
                    continue
            rgb.append(rgbRow)
        return np.array(rgb).astype('uint8')

    # recalculate objects rgb values to use all the available palette colors
    #   similiar to calc_rgb_array from RadiometricImage but operates locally
    def increase_detected_object_contrast(self, obj, temps, pal):
        print(temps.shape[0], temps.shape[1])
        rgb_array = []
        step = (obj['max'] - obj['min']) / 256
        if step <= 0:
            raise ValueError
        for j in range(0, temps.shape[0]):
            rgb_row = []
            for i in range(0, temps.shape[1]):
                temp = temps[j, i]
                index = int((temp - obj['min']) / step)
                if index < 0:
                    index = 0
                if index > 255:
                    index = 255
                pix = pal[index]
                rgb_row.append(pix)
            rgb_array.append(rgb_row)
        obj['rgb'] = np.array(rgb_array).astype('uint8')
