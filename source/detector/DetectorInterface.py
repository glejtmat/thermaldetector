# This Python file uses the following encoding: utf-8
import abc
import time
# base class for detector implementations
#   defines basic functions for loading input and handling output of detection
#   one pure virtual function _detect image needs to be realised by derived class
class DetectorInterface():
    __metaclass__ = abc.ABCMeta

    def __init__(self):
        pass

    @abc.abstractmethod
    def _detect_img(self, image):
        raise NotImplementedError

    # get opecv-ready array of image pixel data
    def _get_cvimage(self, radiometric_image):
        radiometric_image.clearResultData()
        if self._image_type == 'thermal':
            return radiometric_image.get_rgb_data()
        if self._image_type == 'visible':
            return radiometric_image.get_visible_data()
        return []

    # load sources for detection
    #   source determines whether thermal or visible will be used
    def load_image(self, images, source):
        self.images = images
        self._image_type = source
        self.__run_detection()

    # start detection
    def detect(self):
        self._image_type = 'thermal'
        self.__run_detection()

    # setters for detection parameters
    def set_threshold(self, threshold):
        self._threshold = threshold

    def set_dist(self, dist):
        self._dist = dist

    def set_env(self, env):
        self._env = env

    # run detection for all loaded sources
    #   call virtual function
    def __run_detection(self):
        if isinstance(self.images, list):
            for image in self.images:
                self._detect_img(image)
        else:
            start = time.time()
            self._detect_img(self.images)
            end = time.time()
            print('time', end - start)
