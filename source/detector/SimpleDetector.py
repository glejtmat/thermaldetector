# This Python file uses the following encoding: utf-8
from source.detector import KeypointFinder, DetectorInterface
from source.utils import fov
import numpy as np
import cv2


# Detection without machine learning
# image analysis based on temperatures, distance and shapes
class SimpleDetector(DetectorInterface.DetectorInterface):

    def __init__(self):
        DetectorInterface.DetectorInterface.__init__(self)
        self.__keypoint_finder = KeypointFinder.KeypointFinder()

    def __process_keypoints(self):
        for keypoint in self.__keypoints:
            pass

    def _detect_img(self, image):
        print('simple detect start')
        imarray = self._get_cvimage(image)
        pixSize = fov.get_pix_size(image.get_focal_len(), self._dist)
        keypoints = self.__keypoint_finder.blob_detect(imarray, pixSize)
        if self._image_type == "visible":
            image.set_result_data(keypoints, self._image_type)
            return
        res_keypoints = []
        for point in keypoints:
            if self.__keypoint_finder.temp_filter(
                self.__keypoint_finder.get_keypoint_temps(point, image),
                    self._threshold, self._env):
                temp = np.array(
                    self.__keypoint_finder.get_keypoint_temps(point, image))
                rgb = np.array(
                    self.__keypoint_finder.get_keypoint_rgb(point, image))
                res_point = {'x': point.pt[0], 'y': point.pt[1],
                             'r': point.size, "min": temp.min(axis=None),
                             "max": temp.max(axis=None), "avg": temp.mean(axis=None),
                             "rgb": rgb}
                try:
                    gray = cv2.cvtColor(res_point['rgb'], cv2.COLOR_RGB2GRAY)
                except TypeError:
                    continue
                ret, thresh = cv2.threshold(gray, 127, 255, 0)
                contours, hierarchy = cv2.findContours(
                    thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
                if len(contours) == 0:
                    continue
                res_contour = np.vstack([cont for cont in contours])
                #for contour in contours:
                if not self.__keypoint_finder.contour_filter(res_contour):
                    print('filtered out', flush=True)
                    continue

                print(contours, flush=True)
                print('xxxxxxxxxx', flush=True)
                print(res_contour, flush=True)
                cv2.drawContours(
                    res_point['rgb'],[res_contour], -1, (0, 255, 0), 3)
                res_point['contour'] = [res_contour]
                res_keypoints.append(res_point)

        image.set_result_data(res_keypoints, self._image_type)
        print('simple detect done', flush=True)
