# This Python file uses the following encoding: utf-8
from PySide2 import QtWidgets
import numpy as np

# Dimensionless descriptors
class ShapeDescriptors:
    @staticmethod
    def form_factor(area, perimeter):
        return (4 * np.pi * area) / (perimeter * perimeter)

    @staticmethod
    def roundness(area, max_diameter):
        return (4 * area) / (np.pi * max_diameter * max_diameter)

    @staticmethod
    def aspect_ratio(min_diameter, max_diameter):
        return min_diameter / max_diameter;

    @staticmethod
    def convexity(perimeter, convex_perimeter):
        return convex_perimeter / perimeter

    @staticmethod
    def solidity(area, convex_area):
        return area / convex_area

    @staticmethod
    def compactness(area, max_diameter):
        return np.sqrt(4 / np.pi * area) / max_diameter;

    @staticmethod
    def extent(area, bounding_rectangle_area):
        return area / bounding_rectangle_area;
