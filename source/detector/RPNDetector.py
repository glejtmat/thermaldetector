# This Python file uses the following encoding: utf-8
from source.detector import DetectorInterface, KeypointFinder
import numpy as np
from detectron2 import model_zoo
from detectron2.utils.visualizer import ColorMode
from detectron2.config import get_cfg
from detectron2.engine import DefaultPredictor
from detectron2.data import MetadataCatalog
from detectron2.utils.visualizer import Visualizer


# Faster - RCNN detector using Pytorch and Detectron
class RPNDetector(DetectorInterface.DetectorInterface):
    def __init__(self):
        DetectorInterface.DetectorInterface.__init__(self)
        self.__keypoint_finder = KeypointFinder.KeypointFinder()

    def _detect_img(self, radiometric_image):
        # load trained model and set configuration
        model_path = "detection_helper_files/rpn/model_final.pth"
        cfg = get_cfg()
        cfg.merge_from_file(model_zoo.get_config_file("COCO-Detection/faster_rcnn_R_50_FPN_3x.yaml"))
        cfg.DATASETS.TRAIN = ("Thermal_train",)
        cfg.MODEL.WEIGHTS = model_path
        cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.7
        cfg.DATALOADER.NUM_WORKERS = 0
        cfg.SOLVER.IMS_PER_BATCH = 2
        cfg.SOLVER.BASE_LR = 0.00025
        cfg.SOLVER.MAX_ITER = 3000
        cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 4096
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = 6
        # use cpu for inference
        cfg.MODEL.DEVICE='cpu'
        # use predictor to get result and process outputs
        predictor = DefaultPredictor(cfg)

        image = self._get_cvimage(radiometric_image)
        outputs = predictor(image)

        res_keypoints = []
        for box in outputs["instances"].pred_boxes:
            x = box[0].item()
            y = box[1].item()
            center_x = (box[0].item() + box[2].item()) / 2
            center_y = (box[1].item() + box[3].item()) / 2
            w = box[2].item() - box[0].item()
            h = box[2].item() - box[0].item()
            temp = np.array(
                self.__keypoint_finder.get_boundingbox_temps([x, y, w, h], radiometric_image))
            rgb = np.array(
                self.__keypoint_finder.get_boundingbox_rgb([x, y, w, h], radiometric_image))
            res_point = {'x': center_x, 'y': center_y,
                'r': w if w > h else h, "min": temp.min(axis=None),
                "max": temp.max(axis=None), "avg": temp.mean(axis=None),
                "rgb": rgb}
            res_keypoints.append(res_point)
        radiometric_image.set_result_data(res_keypoints, self._image_type)

