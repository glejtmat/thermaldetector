# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QMainWindow
from source.utils import uiloader
from PySide2.QtCore import QObject, Signal, Slot, Qt


class MainWindowWidget(QMainWindow):
    closing = Signal()

    def __init__(self, uifile):
         QMainWindow.__init__(self)
         uiloader.loadUi(uifile, self)

    def closeEvent(self,event):
        self.closing.emit()
