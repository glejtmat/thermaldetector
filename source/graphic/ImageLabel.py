# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QLabel
from PySide2.QtCore import Signal


# QLabel with overriden methods
#   make behaviour suitable to use as pixmap holder
class ImageLabel(QLabel):
    mousePositionChanged = Signal(object, object)

    def __init__(self, *args, **kwargs):
        QLabel.__init__(self, *args, **kwargs)
        self.setMouseTracking(True)
        self._scale = 2

    def mouseMoveEvent(self, event):
        self.mousePositionChanged.emit(event.x(), event.y())

    def wheelEvent(self, event):
        print('wheel', event.delta(), flush=True)

    def setPixmap(self, pixmap):
        pixmap.scaled(pixmap.width() * self._scale,
                      pixmap.height() * self._scale)
        super().setPixmap(pixmap)
