# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QGraphicsScene
from PySide2.QtGui import QImage, QPixmap
from PySide2.QtCore import Signal

# image scene, reimplemented to enable mouse tracking and zoom
# draws the image fiven as numpy array with rgb data
class ImageScene(QGraphicsScene):
    mousePosChanged = Signal(int, int)

    def __init__(self, width, height):
        QGraphicsScene.__init__(self)
        self.__mapped_width = width
        self.__mapped_height = height
        self.setSceneRect(0,0, width, height)


    def set_mapped_size(self, width, height):
        self.__mapped_width = width
        self.__mapped_height = height
        self.setSceneRect(0,0, width, height)

    def draw(self, imarray):
        self.clear()
        self.__drawImage(imarray)

    def __drawImage(self, imarray):
        image = QImage(imarray, imarray.shape[1], imarray.shape[0],
                   imarray.shape[1] * 3, QImage.Format_RGB888)
        pixmap = QPixmap(image)
        self.addPixmap(pixmap.scaled(self.__mapped_width, self.__mapped_height));

    def mouseMoveEvent(self, event):
        self.mousePosChanged.emit(event.scenePos().x(), event.scenePos().y())


