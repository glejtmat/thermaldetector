# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QLabel

# label that shows object preview on detection tab
class KeypointLabel(QLabel):
    def __init__(self, *args, **kwargs):
        QLabel.__init__(self, *args, **kwargs)

    def setPixmap(self, pixmap):
        pixmap = pixmap.scaled(self.width(), self.height())
        super().setPixmap(pixmap)
