# This Python file uses the following encoding: utf-8
from PySide2.QtWidgets import QGraphicsView
from PySide2.QtCore import Slot, Signal, Qt

# image view with custom signal
class ImageView(QGraphicsView):
    imageZoom = Signal(float)

    def __init__(self, *args, **kwargs):
        QGraphicsView.__init__(self, *args, **kwargs)


