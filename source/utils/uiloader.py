# This Python file uses the following encoding: utf-8
from __future__ import (print_function, division, unicode_literals,
                        absolute_import)

from PySide2.QtCore import QFile
from PySide2 import QtUiTools
from source.graphic import ImageLabel, KeypointLabel, ImageView

import os
import sys

from PySide2.QtCore import Slot, QMetaObject
from PySide2.QtUiTools import QUiLoader

SCRIPT_DIRECTORY = os.path.dirname(os.path.abspath(__file__))

# this implementation is from https://gist.github.com/cpbotha/1b42a20c8f3eb9bb7cb8
# I did not create the core for UiLoader
# class that enables loading components from ui file directly into the widget
class UiLoader(QUiLoader):
    """
    Subclass :class:`~PySide.QtUiTools.QUiLoader` to create the user interface
    in a base instance.
    Unlike :class:`~PySide.QtUiTools.QUiLoader` itself this class does not
    create a new instance of the top-level widget, but creates the user
    interface in an existing instance of the top-level class.
    This mimics the behaviour of :func:`PyQt4.uic.loadUi`.
    """

    def __init__(self, baseinstance, customWidgets=None):

        QUiLoader.__init__(self, baseinstance)
        self.baseinstance = baseinstance
        self.customWidgets = customWidgets

    def createWidget(self, class_name, parent=None, name=''):
        if parent is None and self.baseinstance:
            # supposed to create the top-level widget, return the base instance
            # instead
            return self.baseinstance

        else:
            if class_name in self.availableWidgets():
                # create a new widget for child widgets
                widget = QUiLoader.createWidget(self, class_name, parent, name)

            else:
                # if not in the list of availableWidgets, must be a custom widget
                # this will raise KeyError if the user has not supplied the
                # relevant class_name in the dictionary, or TypeError, if
                # customWidgets is None
                try:
                    widget = self.customWidgets[class_name](parent)

                except (TypeError, KeyError) as e:
                    raise Exception('No custom widget ' + class_name + ' found in customWidgets param of UiLoader __init__.')

            if self.baseinstance:
                # set an attribute for the new child widget on the base
                # instance, just like PyQt4.uic.loadUi does.
                setattr(self.baseinstance, name, widget)

                # this outputs the various widget names, e.g.
                # sampleGraphicsView, dockWidget, samplesTableView etc.
                #print(name)

            return widget


def loadUi(uifile, baseinstance=None, customWidgets=None,
           workingDirectory=None):

    loader = UiLoader(baseinstance, customWidgets)

    if workingDirectory is not None:
        loader.setWorkingDirectory(workingDirectory)

    widget = loader.load(uifile)
    QMetaObject.connectSlotsByName(widget)
    return widget

def load_ui_form(filename):
    ui_loader = QtUiTools.QUiLoader()
    ui_loader.registerCustomWidget(ImageLabel.ImageLabel)
    ui_loader.registerCustomWidget(KeypointLabel.KeypointLabel)
    ui_loader.registerCustomWidget(ImageView.ImageView)
    ui_file = QFile(filename)
    form = ui_loader.load(ui_file)
    return form
