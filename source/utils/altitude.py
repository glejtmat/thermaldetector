# This Python file uses the following encoding: utf-8
from json import loads
from urllib.request import Request, urlopen, HTTPError, URLError


# get altitude for given coordinates using Google Maps API
def altitude(lat, lng):
    print(('sending alt request to google maps {0} {1}').format(lat, lng))
    try:
        request = Request((
            'https://maps.googleapis.com/maps/api/elevation/'
            'json?locations={0},{1}&key='
            'AIzaSyAVgYShxAybwQzxH-6iJSPk1mWOc496b94')  # server key
            .format(lat, lng))
        response = urlopen(request).read()
        return loads(response)['results'][0]['elevation']
    except HTTPError:
        print('request failed')
    except URLError:
        print('connection failed')
