# This Python file uses the following encoding: utf-8
import math

# field of view calculator
#   calculated from lens type which is saved to source exif
wiris_res = {
 'hres': 640, 'vres': 512,
}

wiris_lenses = {
    9:  {'hfov': 69, 'vfov': 56, 'ifov': 1.889, 'fmin':  70},
    13: {'hfov': 45, 'vfov': 37, 'ifov': 1.308, 'fmin': 150},
    19: {'hfov': 32, 'vfov': 26, 'ifov': 0.895, 'fmin': 200},
    35: {'hfov': 18, 'vfov': 14, 'ifov': 0.486, 'fmin': 600}}


def getTanDeg(deg):
    rad = deg * math.pi / 180
    return math.tan(rad)

# calculate pixel size from the type of lens and the distance
def get_pix_size(focal_len, distance):
    try:
        return wiris_lenses[int(focal_len)]['ifov'] / 1000 * distance
    except:
        raise AttributeError

# horizontal size of view
def get_hfov(focal_len, distance):
    return 2 * distance * getTanDeg(wiris_lenses[int(focal_len)]['hfov'] / 2)

# verical size of view
def get_vfov(focal_len, distance):
    return 2 * distance * getTanDeg(wiris_lenses[int(focal_len)]['vfov'] / 2)
