# This Python file uses the following encoding: utf-8

# loading palettes from .plt files into list of rgb colors
class PaletteHandler:
    def __init__(self):
        pass
    # get list of 256 colors used as palette
    def get_palette(self, name):
        res = []
        try:
            filename = "source/palettes/" + name.capitalize() + ".plt"
            file = open(filename)
            text = file.readlines()
            # each line is one RGB color with 8 bit per channel
            for line in text:
                line = line.replace("\t", "")
                line = line.replace("\n", "")
                rgb = line.split(";")
                rgb.pop()
                for i in range(0, 3):
                    rgb[i] = int(rgb[i])
                res.append(rgb)
        except FileNotFoundError:
            print('palette not found')
            raise ValueError
        except IndexError:
            print('palette reading error', name, flush=True)
            raise ValueError
        return res
