# This Python file uses the following encoding: utf-8
from source.app import FileHandler, ImageList, DetectionHandler
from source.controllers import MainWindowController, ImageListWidgetController
from source.controllers import ToolbarWidgetController, CentralWidgetController
from source.controllers import FileInfoController, DetectorWidgetController
from source.utils import uiloader
from source.graphic import MainWindowWidget

# Singleton class implemented using nested class
class BeanPool:
    instance = None
    # create instance if __init__ was not called before
    def __init__(self):
        if not BeanPool.instance:
            BeanPool.instance = BeanPool.__BeanPool()
    # delegate to nested class
    def __getattr__(self, name):
        return getattr(self.instance, name)

    class __BeanPool:
        def __init__(self):
            # init backend classes
            self.__file_handler = FileHandler.FileHandler()
            self.__image_list = ImageList.ImageList()
            self.__detectionHandler = DetectionHandler.DetectionHandler()
            # load forms
            self.__window = MainWindowWidget.MainWindowWidget("ui/mainwindow.ui")
            self.__central_widget_form\
                = uiloader.load_ui_form("ui/centralwidget.ui")
            # load central widget into the window
            self.__window.setCentralWidget(self.__central_widget_form)
            self.__init_controllers()
            self.__init_connections()

        def get_file_handler(self):
            return self.__file_handler

        def get_detector_controller(self):
            return self.__detector_controller

        def get_image_list(self):
            return self.__image_list

        def get_window(self):
            return self.__window

        def get_central_controller(self):
            return self.__central_widget_controller

        def get_window_controller(self):
            return self.__window_controller

        def get_central_widget(self):
            return self.__central_widget_form

        # init GUI contollers
        def __init_controllers(self):
            self.__window_controller = MainWindowController.\
                MainWindowController(
                    self.__window, self.__file_handler, self.__image_list)
            self.__central_widget_controller = CentralWidgetController.\
                CentralWidgetController(
                    self.__central_widget_form, self.get_selected_image_temp)
            self.__image_list_controller = ImageListWidgetController.\
                ImageListWidgetController(self.__central_widget_form)
            self.__toolbar_controller = ToolbarWidgetController.\
                ToolbarWidgetController(self.__central_widget_form)
            self.__file_info_controller = FileInfoController.\
                FileInfoController(self.__central_widget_form)
            self.__detector_controller = DetectorWidgetController.\
                DetectorWidgetController(self.__central_widget_form)

        # Connect signals to slots
        #   connects gui controllers with backend classes
        def __init_connections(self):
            # handle image manipulation
            self.__window_controller.images_loaded.connect(
                self.__image_list.add_images)
            self.__window_controller.action_run_triggered.connect(
                lambda all: self.__detectionHandler.start_detector(
                    all, self.__detector_controller.get_alg_opt(),
                    'thermal'))
            self.__image_list_controller.image_selected.connect(
                self.__image_list.select_image)
            self.__image_list.image_list_changed.connect(
                self.__image_list_controller.refresh_image_list)
            self.__image_list.selected_image_changed.connect(
                self.__central_widget_controller.show_image)
            self.__image_list.selected_image_changed.connect(
                lambda: self.__toolbar_controller.on_image_selected(
                    self.__image_list.get_selected_image()))
            self.__image_list.selected_image_changed.connect(
                lambda: self.__detector_controller.on_image_selected(
                    self.__image_list.get_selected_image()))
            self.__image_list.selected_image_changed.connect(
                lambda: self.__toolbar_controller.toggle_IR())
            self.__image_list.selected_image_changed.connect(
                lambda: self.__file_info_controller.refresh_file_info(
                        self.__image_list.get_selected_image()))
            self.__image_list.selected_idx_changed.connect(
                self.__image_list_controller.highlight_row)
            # handle empty image list
            self.__image_list.empty_list.connect(
                self.__central_widget_controller.clearWidget)
            self.__image_list.empty_list.connect(
                self.__detector_controller.clearWidget)
            # handle toolbar inputs
            self.__detector_controller.run_button_clicked.connect(
                self.__detectionHandler.start_detector)
            self.__toolbar_controller.next_button_clicked.connect(
                self.__image_list.select_next_image)
            self.__toolbar_controller.prev_button_clicked.connect(
                self.__image_list.select_prev_image)
            # clear detected objects
            self.__window_controller.action_reset_triggered.connect(
                self.__detectionHandler.clear_detected_data)
            # handle window closing
            self.__window.closing.connect(self.__file_handler.cancel_load)
            self.__window.closing.connect(self.__detectionHandler.cancel_detect)
            self.__window.closing.connect(self.__toolbar_controller.cancel_load)

        def get_selected_image_temp(self, x, y):
            try:
                return self.__image_list.get_selected_image()\
                    .get_temperature_at(x, y)
            except AttributeError:
                return 0
