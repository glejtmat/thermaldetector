# This Python file uses the following encoding: utf-8
from PySide2 import QtCore


class DetectorWorker(QtCore.QThread):
    updateProgress = QtCore.Signal(int)
    workDone = QtCore.Signal(object)

    def __init__(self, detector, list, auto_adjust):
        QtCore.QThread.__init__(self)
        self.__detector = detector
        self.__list = list
        self.__auto_adjust = auto_adjust
        self.__run_flag = True

    def stop(self):
        self.__run_flag = False

    def run(self):
        if self.__auto_adjust:
            for image in self.__list:
                image.auto_adjust()
        self.__detector.load_image(self.__list, 'thermal')
        self.workDone.emit('thermal')
