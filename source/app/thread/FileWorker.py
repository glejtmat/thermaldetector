# This Python file uses the following encoding: utf-8
from PySide2 import QtCore
import numpy as np
import os
from PIL import Image
from source.app import RadiometricImage


class FileWorker(QtCore.QThread):
    updateProgress = QtCore.Signal(int)
    workDone = QtCore.Signal(object)

    def __init__(self, dirpath, listdir, exif_fun, annotation_fun):
        QtCore.QThread.__init__(self)
        self.__run_flag = True
        self.__list_dir = listdir
        self.__dirpath = dirpath
        self.__exif_fun = exif_fun
        self.__annotation_fun = annotation_fun

    def stop(self):
        self.__run_flag = False

    def run(self):
        allF = self.__list_dir
        res = []
        i = 0
        for file in allF:
            if not self.__run_flag:
                self.workDone.emit([])
                return
            i += 1
            self.updateProgress.emit(i)
            if file.endswith(".tiff"):
                imarray = Image.open(self.__dirpath + "/" + os.path.basename(file))
                visarray = []
                try:
                    visarray = Image.open(self.__dirpath + "/" + os.path.basename(file).replace(
                        "radiometric.tiff", "visible.jpg"))
                except IOError:
                    print('file not found')
                exif_data = self.__exif_fun(
                    self.__dirpath + "/" + os.path.basename(file))
                image = RadiometricImage.RadiometricImage(
                    self.__dirpath + "/" + os.path.basename(file), os.path.basename(file),
                    np.array(imarray), np.array(visarray), exif_data)
                self.__annotation_fun(image)
                res.append(image)
        self.workDone.emit(res)
