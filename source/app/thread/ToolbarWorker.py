# This Python file uses the following encoding: utf-8
from PySide2 import QtCore


class ToolbarWorker(QtCore.QThread):
    updateProgress = QtCore.Signal(int)
    workDone = QtCore.Signal()

    def stop(self):
        self.__run_flag = False

    def __init__(self, list, pal, min, max, mask_type, mask):
        QtCore.QThread.__init__(self)
        self.__list = list
        self.__pal = pal
        self.__max = max
        self.__min = min
        self.__mask_type = mask_type
        self.__mask = mask
        self.__run_flag = True

    def run(self):
        i = 0
        for image in self.__list:
            if not self.__run_flag:
                self.workDone.emit()
                return
            self.updateProgress.emit(i)
            i += 1
            image.set_range(
                self.__min, self.__max, self.__pal,
                self.__mask_type, self.__mask)
        self.workDone.emit()
