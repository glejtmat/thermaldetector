# This Python file uses the following encoding: utf-8

from PySide2.QtCore import QObject, Slot, Signal

# container for radiometric image instances
class ImageList(QObject):
    # Signal
    image_list_changed = Signal(object)
    selected_image_changed = Signal(object, object)
    selected_idx_changed = Signal(object)
    empty_list = Signal()

    def __init__(self):
        QObject.__init__(self)
        self.__imageList = []
        self.__selected_image_idx = -1

    # add image, check for duplicity
    @Slot(object)
    def add_images(self, images):
        if isinstance(images, list):
            l = []
            for image in images:
                if not image.get_stats()['filename'] in [i.get_stats()['filename'] for i in self.__imageList]:
                    l.append(image)
            self.__imageList.extend(l)
        else:
            if images.get_stats()['filename'] in [i.get_stats()['filename'] for i in self.__imageList]:
                return
            self.__imageList.append(images)
        self.image_list_changed.emit(self.__imageList)
        # if there was no an image selected before, select first one
        if len(self.__imageList) > 0 and not hasattr(self, '__selected_image'):
            self.select_image(0)
            self.__selected_image_idx = 0

    @Slot(object)
    def get_selected_image(self):
        return self.__selected_image

    # select image from list and notify gui to load
    @Slot(object)
    def select_image(self, index):
        if index < 0 or index >= len(self.__imageList):
            return
        self.__selected_image_idx = index
        self.__selected_image = self.__imageList[index]
        # check if detection was completed, if yes show result data
        if self.__selected_image.detection_done():
            self.selected_image_changed.emit(
                self.__selected_image.get_rgb_data(),
                self.__selected_image.get_stats())
        else:
            self.selected_image_changed.emit(
                self.__selected_image.get_rgb_data(),
                self.__selected_image.get_stats())
        self.selected_idx_changed.emit(self.__selected_image_idx)
    # trigger signal to notify gui abot image changes when changind from outside the list
    def trigger_image_changed(self):
        self.selected_image_changed.emit(
            self.__selected_image.get_rgb_data(),
            self.__selected_image.get_stats())
    # return whole list for batch procesing
    def get_all_images(self):
        return self.__imageList

    @Slot()
    def select_next_image(self):
        if(self.__selected_image_idx < 0 or
                self.__selected_image_idx > len(self.__imageList) - 2):
            return
        self.select_image(self.__selected_image_idx + 1)

    @Slot()
    def select_prev_image(self):
        if(self.__selected_image_idx <= 0 or
                self.__selected_image_idx > len(self.__imageList) - 1):
            return
        self.select_image(self.__selected_image_idx - 1)

    @Slot(object)
    def remove_image(self, index):
        self.__imageList.pop(index)
        self.image_list_changed.emit(self.__imageList)
    # remove image that is currently selected
    @Slot(object)
    def remove_current_image(self):
        if(self.__selected_image_idx < 0 or
                self.__selected_image_idx >= len(self.__imageList)):
            return
        self.__imageList.pop(self.__selected_image_idx)
        self.image_list_changed.emit(self.__imageList)
        listLen = len(self.__imageList)
        # other image needs to be selected, select provious
        if listLen > 0:
            if self.__selected_image_idx >= listLen:
                self.select_image(self.__selected_image_idx - 1)
            else:
                self.select_image(self.__selected_image_idx)
        else:
            self.empty_list.emit()
    # clear image list
    @Slot(object)
    def remove_all(self):
        self.__imageList.clear()
        self.image_list_changed.emit(self.__imageList)
        self.empty_list.emit()
