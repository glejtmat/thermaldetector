# This Python file uses the following encoding: utf-8
import os
import numpy as np
from json import dump, load, JSONDecodeError
from io import UnsupportedOperation
from PIL import Image
import PIL.ExifTags
from PySide2.QtWidgets import QFileDialog
from source.app import RadiometricImage, BeanPool
from source.utils import altitude
from source.app.thread import FileWorker
from source.app import ImageInfo, global_defs
import piexif
from PIL.TiffTags import TAGS
import exifread

# File and directory reading into RadiometricImage instances
class FileHandler:
    def __init__(self):
        pass

    # load file into RadiometricImage instance
    def load_file(self, path):
        imarray = Image.open(path)
        visarray = Image.open(path.replace(
            "radiometric.tiff", "visible.jpg"))
        exif_data = self.__readExif(path)
        image = RadiometricImage.RadiometricImage(
            path, os.path.basename(path),
            np.array(imarray), np.array(visarray), exif_data)
        self.__read_result_anotation(image)
        return image

    # start thread for reading multiple files
    def load_files(self, paths):
        self.cancel_load()
        # connect to progress bar
        self.__progressBar = BeanPool.BeanPool().\
            get_central_controller().get_progress_bar()
        self.thread = FileWorker.FileWorker(
            os.path.dirname(paths[0]), paths, self.__readExif,
            self.__read_result_anotation)
        self.thread.updateProgress.connect(
            lambda progress: self.__progressBar.setValue(progress))
        self.thread.workDone.connect(
            BeanPool.BeanPool().
            get_window_controller().handle_action_load_dir_done)
        self.thread.workDone.connect(
            lambda: BeanPool.BeanPool().get_central_controller().
            hide_progress_bar())
        # show progress bar before start
        BeanPool.BeanPool().get_central_controller().show_progress_bar('file')
        self.__progressBar.setMinimum(0)
        self.__progressBar.setValue(0)
        self.__progressBar.setMaximum(len(paths))
        BeanPool.BeanPool().get_central_controller().cancelLoad\
            .connect(self.cancel_load)
        self.thread.start()

    # start thread for reading multiple files from directory
    def load_dir(self, dirpath):
        allF = os.listdir(dirpath)
        self.__progressBar = BeanPool.BeanPool().\
            get_central_controller().get_progress_bar()
        self.thread = FileWorker.FileWorker(
            dirpath, allF, self.__readExif,
            self.__read_result_anotation)
        self.thread.updateProgress.connect(
            lambda progress: self.__progressBar.setValue(progress))
        self.thread.workDone.connect(
            BeanPool.BeanPool().
            get_window_controller().handle_action_load_dir_done)
        self.thread.workDone.connect(
            lambda: BeanPool.BeanPool().get_central_controller().
            hide_progress_bar())
        BeanPool.BeanPool().get_central_controller().show_progress_bar('file')
        self.__progressBar.setMinimum(0)
        self.__progressBar.setRange(0, len(allF))
        BeanPool.BeanPool().get_central_controller().cancelLoad\
            .connect(self.cancel_load)
        self.thread.start()

    # stop thread
    def cancel_load(self):
        try:
            if not self.thread.isRunning():
                return
        except TypeError:
            return
        self.thread.stop()
        BeanPool.BeanPool().get_central_controller().hide_progress_bar()


    # save tiff to location on disk
    def save_file_as(self, image):
        # create dialog
        path = QFileDialog.getSaveFileName(
            None, "Save", "~/Desktop/", "Image (*.tiff))")[0]
        path += "-export.jpeg"
        print('saving', path, flush=True)
        imarray = image.get_rgb_data()
        try:
            Image.fromarray(imarray).save(path)
        except ValueError:
            print('unable to save')
        # save also visible image if available
        visarray = image.get_visible_data()
        if len(visarray) > 0:
            im = Image.fromarray(visarray)
            try:
                im.save(path.replace("radiometric.tiff", "visible-export.jpg"))
            except ValueError:
                print('unable to save')

    # save file to original location
    def save_file(self, image):
        stats = image.get_stats()
        path = stats['filepath']
        print('saving', path, flush=True)
        imarray = image.get_rgb_data()
        try:
            pil_image = Image.fromarray(imarray)
            pil_image.save(path.replace(".tiff", "-export.jpg"))
        except ValueError:
            print('unable to save')
        # save also visible image if available
        visarray = image.get_visible_data()
        if len(visarray) > 0:
            im = Image.fromarray(visarray)
            try:
                im.save(path.replace("radiometric.tiff", "-exportvisible.jpg"))
            except ValueError:
                print('unable to save')
        # save result if detected
        if image.detection_done():
            self.__save_result_anotation(image)

    # save result to json object
    def __save_result_anotation(self, image):
        with open(
            image.get_stats()['filepath'].replace(".tiff", ".json"), 'w')\
                as outfile:
            keypoints = image.get_detected_objects()
            dump(([{'x': point['x'], 'y': point['y'],
                    'r': point['r'], 'min': point['min'],
                    'avg': point['avg'], 'max': point['max']}
                 for point in keypoints]), outfile)

    # read result from json object
    def __read_result_anotation(self, image):
        path = image.get_stats()['filepath']
        try:
            with open(path.replace(".tiff", ".json"), 'r') as infile:
                image.set_result_data_from_JSON(load(infile))
        except UnsupportedOperation:
            print('json anotation not found')
        except FileNotFoundError:
            print('json anotation not found')
        except JSONDecodeError:
            print('json anotation invalid')

    def delete_result_anotation(self, image):
        path = image.get_stats()['filepath']
        try:
            os.remove(path.replace(".tiff", ".json"))
        except UnsupportedOperation:
            print('json anotation not found')
        except FileNotFoundError:
            print('json anotation not found')
    # read exif metadata from tiff source
    #   load data into ImageInfo instance
    def __readExif_tiff(self, path):
        f = open(path, 'rb')
        tags = exifread.process_file(f)
        image_info = ImageInfo.ImageInfo()
        for tag in tags.keys():
            if tag not in ('JPEGThumbnail', 'TIFFThumbnail', 'Filename', 'EXIF MakerNote'):
                if tag == 'GPS GPSLatitude':
                    image_info.latitude = tags[tag].values[0].num / tags[tag].values[0].den
                if tag == 'GPS GPSLongitude':
                    image_info.longitude = tags[tag].values[0].num / tags[tag].values[0].den
                if tag == 'GPS GPSLatitudeRef':
                    image_info.latitude_ref = tags[tag].values
                if tag == 'GPS GPSLongitudeRef':
                    image_info.longitude_ref = tags[tag].values
                if tag == 'GPS GPSAltitude':
                    image_info.altitude = tags[tag].values[0].num / tags[tag].values[0].den
                if tag == 'Image Model':
                    image_info.model = tags[tag].values
                if tag == 'Image Make':
                    image_info.make = tags[tag].values
                if tag == 'EXIF FocalLength':
                    image_info.focal_length = tags[tag].values[0].num / tags[tag].values[0].den
        image_info.altitude = global_defs.altitudeCorrection(image_info.model, image_info.altitude)
        image_info.distance = image_info.altitude - altitude.altitude(
            image_info.latitude,
            image_info.longitude)
        return image_info
    # read exif metadata from jpeg source
    #   load data into ImageInfo instance
    def __readExif_jpeg(self, path):
        print('read exif jpeg', path)
        im = []
        try:
            im = Image.open(path)
        except AttributeError:
            print('exif err ', path)
            raise
        except FileNotFoundError:
            print('exif not found ', path)
            raise AttributeError
        except IOError:
            print('radiometric jpeg reading error')
            raise AttributeError
        image_info = ImageInfo.ImageInfo()
        for k, v in im._getexif().items():
            if k in PIL.ExifTags.TAGS:
                tag = PIL.ExifTags.TAGS[k]
                if tag == 'GPSInfo':
                    image_info.latitude = v[2][0][0] / v[2][0][1]
                    image_info.longitude = v[4][0][0] / v[4][0][1]
                    image_info.latitude_ref = v[1]
                    image_info.longitude_ref = v[3]
                    image_info.altitude = v[6][0] / v[6][1]
                if tag == 'Model':
                    image_info.model = v
                if tag == 'Make':
                    image_info.make = v
                if tag == 'FocalLength':
                    image_info.focal_length = v[0] / v[1]
                if tag == 'CameraSerialNumber':
                    image_info.serial = v
        image_info.distance = image_info.altitude - altitude.altitude(
            image_info.latitude,
            image_info.longitude)
        return image_info
        print('read exif jpeg done', path)

    # read exif
    def __readExif(self, path):
        #try reading jpeg and then tiff
        try:
            return self.__readExif_jpeg(path.replace(".tiff", ".jpg"))
        except AttributeError:
            print('jpef exif error')
            return self.__readExif_tiff(path)

