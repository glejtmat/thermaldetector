# This Python file uses the following encoding: utf-8
from PySide2 import QtWidgets

# metadata structure
class ImageInfo:
    latitude = 0
    longitude = 0
    altitude = 0
    distance = 0
    model = ""
    make = ""
    serial = ""
    focal_length = 0
    timestamp = ""
    longitude_ref = ""
    latitude_ref = ""

    def __init__(self):
        pass
