# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject
from source.detector import SimpleDetector, YoloDetector
from source.detector import RPNDetector
from source.app import BeanPool
from source.app.thread import DetectorWorker

# Detection handler manages detection
#    holds detector instances and prompts detection
class DetectionHandler(QObject):
    def __init__(self):
        QObject.__init__(self)
        self.__init_detectors()

    def start_detector(self, all, type, auto_adjust):
        self.cancel_detect()
        pool = BeanPool.BeanPool()
        self.__load_detector_params(type)
        self.__progressBar = BeanPool.BeanPool().\
            get_central_controller().get_progress_bar()
        try:
            if all:
                self.thread = DetectorWorker.DetectorWorker(
                    self.__detectors[type],
                    pool.get_image_list().get_all_images(), auto_adjust)
            else:
                self.thread = DetectorWorker.DetectorWorker(
                    self.__detectors[type],
                    [pool.get_image_list().get_selected_image()], auto_adjust)
        except AttributeError:
            return
        self.thread.workDone.connect(
            self.__detector_finished)
        self.thread.workDone.connect(
            lambda: BeanPool.BeanPool().get_central_controller().
            hide_progress_bar())
        BeanPool.BeanPool().get_central_controller().show_progress_bar('detect')
        self.__progressBar.setRange(0, 0)
        BeanPool.BeanPool().get_central_controller().cancelLoad\
            .connect(self.cancel_detect)
        self.thread.start()

    # stop thread
    def cancel_detect(self):
        try:
            if not self.thread.isRunning():
                return
        except AttributeError:
            return

        self.thread.stop()
        BeanPool.BeanPool().get_central_controller().hide_progress_bar()

    def __detector_finished(self, source):
        pool = BeanPool.BeanPool()
        image = pool.get_image_list().get_selected_image()
        imgdata = image.get_rgb_data()
        pool.get_central_controller().show_image(
            imgdata, image.get_stats())
        pool.get_detector_controller().refresh_result_table(image)
    # load parameters from gui controller
    def __load_detector_params(self, type):
        pool = BeanPool.BeanPool()
        self.__detectors[type].set_threshold(
            pool.get_detector_controller().get_threshold_opt())
        self.__detectors[type].set_env(
            pool.get_detector_controller().get_env_opt())
        self.__detectors[type].set_dist(
            pool.get_detector_controller().get_distance_opt())

    def clear_detected_data(self):
        pool = BeanPool.BeanPool()
        pool.get_file_handler().delete_result_anotation(
            pool.get_image_list().get_selected_image())
        pool.get_image_list().get_selected_image().clearResultData()
        self.__detector_finished('thermal')

    # Init Detector Classes           #
    def __init_detectors(self):
        self.__detectors = {}
        self.__detectors['simple'] = SimpleDetector.SimpleDetector()
        self.__detectors['yolo'] = YoloDetector.YoloDetector()
        self.__detectors['rpn'] = RPNDetector.RPNDetector()
