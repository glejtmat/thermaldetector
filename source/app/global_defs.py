# This Python file uses the following encoding: utf-8
# Constant values used globally in an application
THERMAL_IMAGE_HEIGHT = 512
THERMAL_IMAGE_WIDTH = 640

VISIBLE_IMAGE_HEIGHT = 1080
VISIBLE_IMAGE_WIDTH = 1920

TEMP_THRESHOLD = 30.0

FORM_FACTOR_MIN = 0.02
ROUNDNESS_MIN = 0
CONVEXITY_MIN = 0.05
SOLIDITY_MIN = 0.1
COMPACTNESS_MIN = 0.1
EXTENT_MIN = 0.05
ASPECT_MIN = 0


# corrent altitude saved in exif,
# there is an error in camera M200 and it saves altitude lower by 148.16 meters
def altitudeCorrection(cam_type, alt):
    if "M200" in cam_type:
        return alt + 148.16
    return alt


# converting raw int to temperature
def rawToTemp(raw):
    return raw / 40 - 100
