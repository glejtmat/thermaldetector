# This Python file uses the following encoding: utf-8
from PySide2.QtCore import QObject
import cv2
import numpy as np
from scipy import stats
import operator
from PySide2.QtGui import QImage
from source.app import PaletteHandler
from source.detector import KeypointFinder
from source.app import global_defs

# class that represents one image
#   holds all the data about one source
class RadiometricImage(QObject):
    def __init__(self, path, name, imarray, visarray, exif_info):
        QObject.__init__(self)
        # load pixel data for thermal and visible image
        self.__raw_array = imarray
        self.__temp_array = self.__calc_array_to_temp(self.__raw_array)
        self.__vis_array = visarray
        self.__has_visible = len(visarray) > 0
        self.__filename = name
        self.__filepath = path
        # set default values
        self.__detection_done = False
        self.__detected_objects = []
        self.__exifInfo = exif_info
        # use real min and max temperature as palette at first
        self.__palette_min = round(self.__temp_array.min(), 2)
        self.__palette_max = round(self.__temp_array.max(), 2)
        # set gray as default palette
        self.__palette_name = "gray"
        self.set_palette(self.__palette_name)
        self.__mask = {'type': 'none', 'value': 0}

    # return dictionary with info about files temperatures and detection
    def get_stats(self):
        return {
            'filename': self.__filename,
            'filepath': self.__filepath,
            'resolution': self.__temp_array.shape,
            'sum': round(self.__temp_array.sum(), 2),
            'min': round(self.__temp_array.min(), 2),
            'max': round(self.__temp_array.max(), 2),
            'mean': round(self.__temp_array.mean(), 2),
            'std_deviation': round(np.std(self.__temp_array), 2),
            'detection_done': self.__detection_done,
            'found_objects': len(self.__detected_objects),
            'has_visible': self.__has_visible
        }

    # get ImageInfo instance with metadata from exif
    def get_exif_data(self):
        return self.__exifInfo

    def detection_done(self):
        return self.__detection_done

    # thermal image pixel array as saved to orig image
    def get_orig_data(self):
        return self.__raw_array

    # visible image pixel array
    def get_visible_data(self):
        return self.__vis_array

    # thermal image pixel array with applied settings
    def get_rgb_data(self):
        return self.__rgb_array

    def get_palette_min(self):
        return self.__palette_min

    def get_palette(self):
        return self.__palette

    def get_palette_max(self):
        return self.__palette_max

    def get_palette_name(self):
        return self.__palette_name

    def set_mask(self, type, value):
        self.__calc_rgb_data()
        self.__mask = {'type': type, 'value': value}
        self.__apply_mask()

    # fetch palette colors from palette handler and set to image
    def set_palette(self, name):
        handler = PaletteHandler.PaletteHandler()
        try:
            self.__palette = handler.get_palette(name)
        except ValueError:
            return
        self.__palette_name = name
        self.__calc_rgb_data()

    def set_palette_limits(self, min, max):
        self.__palette_max = max
        self.__palette_min = min
        self.__calc_rgb_data()

    # set all params at once to prevent multiple calculations
    def set_range(self, min, max, name, type, value):
        handler = PaletteHandler.PaletteHandler()
        try:
            self.__palette = handler.get_palette(name)
        except ValueError:
            return
        self.__palette_name = name
        self.__palette_max = max
        self.__palette_min = min
        self.__calc_rgb_data()
        self.__mask = {'type': type, 'value': value}
        self.__apply_mask()

    # load detection results
    def set_result_data(self, found_objects, source):
        for obj in found_objects:
            self.__draw_bounding_box(
                self.__rgb_array,
                round(obj['x'] - obj['r']/2),
                round(obj['y'] - obj['r']/2),
                round(obj['x'] - obj['r']/2 + obj['r']),
                round(obj['y'] - obj['r']/2 + obj['r']))
        self.__detection_done = True
        self.__detected_objects = found_objects
        # write to exif
        self.__exifInfo.detection = {'objects': len(found_objects)}

    # load detection results from JSON file
    def set_result_data_from_JSON(self, json_data):
        finder = KeypointFinder.KeypointFinder()
        self.__detected_objects = json_data
        for obj in self.__detected_objects:
            self.__draw_bounding_box(
                self.__rgb_array,
                round(obj['x'] - obj['r']/2),
                round(obj['y'] - obj['r']/2),
                round(obj['x'] - obj['r']/2 + obj['r']),
                round(obj['y'] - obj['r']/2 + obj['r']))
            rgb = np.array(
                finder.get_keypoint_rgb(cv2.KeyPoint(obj['x'], obj['y'], obj['r']), self))
            obj['rgb'] = rgb


        # write to exif
        self.__exifInfo.detection =\
            {'objects': len(self.__detected_objects)}
        self.__detection_done = True

    # clear detection results
    def clearResultData(self):
        self.__calc_rgb_data()
        self.__detected_objects = []
        self.__detection_done = False
        self.__exifInfo.detection = {'objects': 0}

    # get temperature that is the most common
    def get_env_temp(self):
        rnd_array = self.__temp_array.round(1)
        res = stats.mode(rnd_array, axis=None)
        return res[0][0]

    def get_focal_len(self):
        return (self.__exifInfo.focal_length)

    def get_temperatures(self):
        return self.__temp_array

    def get_temperature_at(self, x, y):
        try:
            res = self.__temp_array[y][x]
            return res
        except IndexError:
            return 0.0

    def get_detected_objects(self):
        return self.__detected_objects

    # apply masking to rgb data
    def __apply_mask(self):
        for j in range(0, self.__temp_array.shape[0]):
            for i in range(0, self.__temp_array.shape[1]):
                if self.__mask['type'] == 'above':
                    if self.__get_truth(
                        self.__temp_array[j, i], operator.ge,
                            self.__mask['value']):
                        self.__rgb_array[j, i] = self.__palette[0]
                if self.__mask['type'] == 'below':
                    if self.__get_truth(
                        self.__temp_array[j, i], operator.le,
                            self.__mask['value']):
                        self.__rgb_array[j, i] = self.__palette[255]

    # calculate thermal rgb pixel array from temperatures and palette
    def __calc_rgb_data(self):
        self.__rgb_array = []
        step = (self.__palette_max - self.__palette_min) / 256
        if step <= 0:
            raise ValueError
        # for each pixel calculate color based on temperature
        # there are 256 color, so each one represents
        #   1/256 of interval between min and max temo
        for j in range(0, self.__temp_array.shape[0]):
            rgb_row = []
            for i in range(0, self.__temp_array.shape[1]):
                temp = self.__temp_array[j, i]
                index = int((temp - self.__palette_min) / step)
                if index < 0:
                    index = 0
                if index > 255:
                    index = 255
                pix = self.__palette[index]
                rgb_row.append(pix)
            self.__rgb_array.append(rgb_row)
        self.__rgb_array = np.array(self.__rgb_array).astype('uint8')

    # auto adjust image to make objects the most distinguishable
    def auto_adjust(self):
        print('auto adjust')
        self.__palette_min = self.get_env_temp()
        self.__palette_max = self.__temp_array.max() if self.__temp_array.max() -  self.get_env_temp() < global_defs.TEMP_THRESHOLD else global_defs.TEMP_THRESHOLD
        self.__calc_rgb_data()

    # draw keypoint areas which are circles
    def __draw_keypoints(self, keypoints, source):
        if source == "thermal":
            self.__rgb_array = cv2.drawKeypoints(
                    self.get_rgb_data(),
                    [(cv2.KeyPoint(x['x'], x['y'], x['r']))
                        for x in keypoints],
                    np.array([]),
                    (255, 0, 0), cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    # draw keypoint areas with rectangular shape
    def __draw_bounding_box(self, img, x, y, x_plus_w, y_plus_h):
        # choose random color
        COLORS = np.random.uniform(0, 255, size=(1, 3))
        color = COLORS[0]
        cv2.rectangle(img, (x, y), (x_plus_w, y_plus_h), color, 2)

    # highlight keypoint when selected from results table
    def highlight_keypoint(self, keypoints, idx, source):
        #self.__draw_keypoints(keypoints, source)
        if source == "thermal":
            self.__rgb_array = cv2.drawKeypoints(
                self.get_rgb_data(), [cv2.KeyPoint(
                    keypoints[idx]['x'], keypoints[idx]['y'],
                    keypoints[idx]['r'])],
                np.array([]), (0, 0, 255),
                cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)

    # compute temperatures from data loaded from tiff
    #   formula given by camera manufacturer
    def __calc_array_to_temp(self, imarray):
        return global_defs.rawToTemp(imarray)

    # helper function that has operator as parameter
    def __get_truth(self, inp, relate, cut):
        return relate(inp, cut)
