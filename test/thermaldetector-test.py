# This Python file uses the following encoding: utf-8
import PySide2.QtTest
import os
import sys
from PIL import Image
from json import load
import numpy as np
import cv2


class ThermalDetectorTest():
    def __init__(self):
        pass

    @staticmethod
    def evaluateDetection(paths, overlap_threshold):
        print("Detection Evaluate Start")
        cnt = 0
        global_success_count = 0
        global_annotated_objects = 0
        global_detected_objects = 0
        for path in paths:
            if not path.endswith(".tiff"):
                continue
            cnt += 1
            try:
                orig_annotation_file = open(path.replace(".tiff", ".txt"), 'r')
            except FileNotFoundError:
                print('\tOriginal Annotation not found ', path)
                continue
            try:
                detected_annotation_file = open(
                    path.replace(".tiff", ".json"), 'r')
            except FileNotFoundError:
                print('\tDetection Annotation not found: ', path)
                continue
            imarray = Image.open(path)
            annotated_objects = ThermalDetectorTest.__process_annotations_file(
                orig_annotation_file, imarray.size[1], imarray.size[0])
            detected_objects = ThermalDetectorTest.__process_detected_file(
                load(detected_annotation_file))
            sorted_detected_objects = []
            for orig_obj in annotated_objects:
                max_overlap = 0
                max_overlap_idx = 0
                i = 0
                for detected_obj in detected_objects:
                    overlap = ThermalDetectorTest.__find_rect_overal(
                        orig_obj, detected_obj)
                    if overlap > max_overlap:
                        max_overlap = overlap
                        max_overlap_idx = i
                    i += 1
                if not max_overlap == 0:
                    sorted_detected_objects.append(
                        detected_objects[max_overlap_idx])
            i = 0
            success_count = 0
            for orig_obj in annotated_objects:
                try:
                    res_overlap = ThermalDetectorTest.__find_rect_overal(orig_obj, sorted_detected_objects[i])
                except IndexError:
                    continue
                res_overlap_inverse = ThermalDetectorTest.__find_rect_overal(sorted_detected_objects[i], orig_obj)
                if res_overlap < overlap_threshold:
                    print('\tObject overlap below threshold: ', res_overlap, ' ', path)
                    continue
                print('\tObject detected, overlaps: {} {}'.format(res_overlap, res_overlap_inverse))
                success_count += 1
                i += 1
            global_success_count += success_count
            global_annotated_objects += len(annotated_objects)
            global_detected_objects += len(detected_objects)
            print('Correctly detected {} of out {} object(s)'.format(success_count, len(annotated_objects)))
            print('--------------------------------------------------------------------------', flush=True)
        print("Total Images:     ", cnt)
        print("Total Score:      ", '{} of out {} object(s)'.format(global_success_count, global_annotated_objects))
        print("False Positives:  ", '{}'.format(global_detected_objects - global_success_count))
        print("Detection Evaluate done")

    @staticmethod
    def __process_annotations_file(file, image_height, image_width):
        annotated_objects = []
        f = file.readlines()
        for line in f:
            obj = line.replace('\n', '').split(' ')
            annotated_objects.append(
                {'x1': round(float(obj[1]) * image_width -  float(obj[3]) * image_width / 2),
                 'y1': round(float(obj[2]) * image_height - float(obj[4]) * image_height / 2),
                 'x2': round(float(obj[1]) * image_width +  float(obj[3]) * image_width / 2),
                 'y2': round(float(obj[2]) * image_height + float(obj[4]) * image_height / 2),
                 'w': round(float(obj[3]) * image_width),
                 'h': round(float(obj[4]) * image_height)
                 })
        return annotated_objects

    @staticmethod
    def __process_detected_file(file):
        annotated_objects = []
        for obj in file:
            annotated_objects.append(
                {'x1': int(obj['x'] - obj['r']),
                 'y1': int(obj['y'] - obj['r']),
                 'x2': int(obj['x'] + obj['r']),
                 'y2': int(obj['y'] + obj['r']),
                 'w': int(obj['x'] + obj['r']) - int(obj['x'] - obj['r']),
                 'h': int(obj['y'] + obj['r']) - int(obj['y'] - obj['r'])})

        return annotated_objects

    @staticmethod
    def __find_rect_overal(A, B):
        SI = max(0, min(A['x2'], B['x2']) - max(A['x1'], B['x1'])) * max(0, min(A['y2'], B['y2']) - max(A['y1'], B['y1']))
        SA = (A['x2'] - A['x1']) * (A['y2'] - A['y1'])
        SB = (B['x2'] - B['x1']) * (B['y2'] - B['y1'])
        SU = SA + SB - SI
        if SA == 0:
            return 0
        return SI/SA


if __name__ == "__main__":
    if(len(sys.argv) == 2):
        path = sys.argv[1]
        files = os.listdir(path)
        ThermalDetectorTest.evaluateDetection([path + '/' + s for s in files], 0.8)
    else:
        print("Usage: thermaldetector-test dir-path")
