# This Python file uses the following encoding: utf-8
import sys
import os
from PySide2 import QtCore
from PySide2.QtWidgets import QApplication
from source.app import BeanPool


# Main class, wrapper for the whole application
class ThermalDetector:
    def __init__(self):
        # use native look
        QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_ShareOpenGLContexts)

    # Start App Routine
    def run(self):
        QApplication.setStyle("mac")
        self.__app = QApplication([])
        # init bean pool with all the app instances
        self.__pool = BeanPool.BeanPool()
        # show window
        self.__pool.get_window().show()
        # check command line args and load if any filename was given
        self.__proces_args()
        sys.exit(self.__app.exec_())

    #  Process Args
    #   filename/dirname
    def __proces_args(self):
        if(len(sys.argv) == 2):
            path = sys.argv[1]
            try:
                if os.path.isdir(path):
                    self.__pool.get_file_handler().load_dir(path)
                else:
                    img = self.__pool.get_file_handler().load_file(path)
                    self.__pool.get_image_list().add_images(img)
            except FileNotFoundError:
                print('Files not found')


# Main Routine
#   create application instance and start it
if __name__ == "__main__":
    thermaldetector = ThermalDetector()
    thermaldetector.run()
