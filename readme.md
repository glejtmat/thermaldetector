Thermal Detector - implementation

This application is part of a bachelor thesis for Software Engineering course of Faculty of Information Technology CTU in Prague.
Main goal of this project is to create desktop application which helps user with detecting objects on thermograms. Design of graphical
interface is inspired by classic image viewers.

Developed with python 3.7.3, Anaconda Distribution.

Required libraries: pillow, numpy, opencv, pieexif, pytorch, tensorflow, exifread

Tested on Debian, MacOS Catalina

Object detection systems: darknet, detectron2

The application contains test dataset in directory "test-dataset".
Full datasets are avalable from Dropbox:

[Session 1 - Winter](https://www.dropbox.com/s/8m16xyzwv4ynakx/dataset-winter.zip?dl=0)
[Session 2 - Spring](https://www.dropbox.com/s/b1b59d8s9mfs7w4/dataset-spring.zip?dl=0)

[Weights files required for inference:](https://www.dropbox.com/s/3vgvxzey2gluwda/detection_helper_files.zip?dl=0)

**Installation Guide**

Install anaconda python distribution with graphical installer from:
https://www.anaconda.com/products/individual

I suggest to create Python virtual environment and installing dependencies into this environment.

To create a virtual environment, decide upon a directory where you want to place it, and run the venv module as a script with the directory path.
python3 -m venv env-name path

Once you’ve created a virtual environment, you may activate it.

On Windows, run:

tutorial-env\Scripts\activate.bat

On Unix or MacOS, run:

source tutorial-env/bin/activate

Activating the virtual environment will change your shell’s prompt to show what virtual environment you’re using, and modify the environment so that running python will get you that particular version and installation of Python.

Then install following packages using pip.

pillow opencv-python-headless numpy PySide2 piexif exifread imutils
e.g. pip install pillow

Detection systems installation:

For YOLO detection and inference, OpenCV is sufficient

Detectron2:
Install pytorch with conda:
conda install pytorch torchvision -c pytorch

Full Pytorch Installation Guide:
https://pytorch.org/get-started/locally

Install detectron from source:
git clone https://github.com/facebookresearch/detectron2.git

Full Detectron Installation Guide:
https://github.com/facebookresearch/detectron2/blob/master/INSTALL.md

After all the depencies were installed, verify the process by running thermaldetector.py.

cd project_parent_directory
python3 thermaldetector.py

To run or develop the app in Qt Creator, load thermaldetector.py project and set Python interpered one from created virtual environment.
In Qt Creator, go to Projects and set Python interpreter in run options.


Detection notes:
Inference using trained neural networks is done on CPU as a default. To infere using GPU, OpenCV with CUDA and CUDNN must be installed.
Installation process:
https://gist.github.com/raulqf/f42c718a658cddc16f9df07ecc627be7

After successful of CUDA instalation, uncomment lines 56, 57 in YOLODetector


Troubleshooting:

Qt Platform Plugin could not be found error
This error is cause by conflicting OpenCV installation, see https://github.com/ageitgey/face_recognition/issues/1041
Make sure you install opencv-python-headless instead of opencv-python to avoid these errors.

Detectron compile errors on MAC OS:
\<wchar\> not found
\<stdlib\> not found

install macports:

install llvm:
sudo port install llvm-9.

Compile detectron with:
MYCC=/opt/local/libexec/llvm-9.0/bin/clang MYCXX=/opt/local/libexec/llvm-9.0/bin/clang++ python -m pip install -e detectron2




